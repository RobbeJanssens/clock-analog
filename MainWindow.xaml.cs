﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace clock {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow( ) {
			InitializeComponent( );
			DispatcherTimer timer = new DispatcherTimer();
			timer.Interval = TimeSpan.FromMilliseconds(10);
			timer.Tick += Timer_Tick;
			timer.Start( );
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
		}

		private void Timer_Tick(object sender, EventArgs e) {
			DateTime now = DateTime.Now;

			double angle = (now.Second + (double)now.Millisecond / 1000) / 60 * 2 * Math.PI - Math.PI / 2;
			secondLine.X2 = 250 + 240 * Math.Cos(angle);
			secondLine.Y2 = 250 + 240 * Math.Sin(angle);
			angle = ((double)now.Second / 60 + now.Minute) / 60 * 2 * Math.PI - Math.PI / 2;
			minuteLine.X2 = 250 + 200 * Math.Cos(angle);
			minuteLine.Y2 = 250 + 200 * Math.Sin(angle);
			angle = ((double)now.Minute / 60 + now.Hour) % 12 / 12 * 2 * Math.PI - Math.PI / 2;
			hourLine.X2 = 250 + 100 * Math.Cos(angle);
			hourLine.Y2 = 250 + 100 * Math.Sin(angle);
		}
	}
}
